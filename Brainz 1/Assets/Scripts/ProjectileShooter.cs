﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileShooter : MonoBehaviour {

    GameObject prefab;
    public float projectileSpeed = 20;
	[SerializeField]
    Camera playerCamera;
	public string playerID;
    // Use this for initialization
    void Start () {
        prefab = Resources.Load("spit projectile") as GameObject;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire"+playerID))
        {
            GameObject projectile = Instantiate(prefab) as GameObject;
			projectile.transform.position = playerCamera.transform.position + playerCamera.transform.forward;
            Rigidbody rb = projectile.GetComponent<Rigidbody>();
			rb.velocity = playerCamera.transform.forward * projectileSpeed;
        }
	}
}
